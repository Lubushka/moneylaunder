package com.example.spin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    // Объявляем
    Button button;
    TextView score;
    ImageView weel;
    EditText vvod, schet;
    Random rd;


    int st = 0, sto = 0, e, scor = 10000, env;
    // Задаём постоянный градус
    private static final float Factor = 4.86f;
    // Задаём название файлу и ключу

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Объявляем
        vvod = (EditText) findViewById(R.id.vvod);
        button = (Button) findViewById(R.id.button);
        weel = (ImageView) findViewById(R.id.weel);
        score = (TextView) findViewById(R.id.score);
        schet = (EditText) findViewById(R.id.schet);

        score.setText(Integer.toString(scor));

        // Случайное число
        rd = new Random();

        // Слушатель кнопки
        button.setOnClickListener(new View.OnClickListener() {
            // При клике
            @Override
            public void onClick(View v) {
                e = Integer.parseInt(vvod.getText().toString());
                // Проверяем правильность ввода
                if (e < 37) {
                    // Делим и возвращаем остаток
                    sto = st % 360;
                    st = rd.nextInt(3600) + 720;
                    // Задаём анимацию вращения
                    RotateAnimation rotate = new RotateAnimation(sto, st,
                            RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
                    // Интервал времени
                    rotate.setDuration(3600);
                    // Сохраняется изменение после анимации
                    rotate.setFillAfter(true);
                    rotate.setInterpolator(new DecelerateInterpolator());
                    // Слушатель анимации
                    rotate.setAnimationListener(new Animation.AnimationListener() {

                        @Override
                        public void onAnimationStart(Animation animation) {
                        }


                        // При окончании анимации присваиваем
                        @Override
                        public void onAnimationEnd(Animation animation) {
                            showToast();

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    // Крутим колесо
                    weel.startAnimation(rotate);
                } else {
                    // Выводим ничего
                }
            }
        });
    }
        // Показываем уведомление
    public void showToast() {
        // Создаём и отображаем текстовое уведомление
        Toast toast = Toast.makeText(getApplicationContext(), cn(360 - (st % 360))
                ,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.show();

        if (cn(360 - (st % 360)) == "Вы выйграли") {
            onWin();
        } else onLose();
    }
        // Сумма при выйгрыше
    public void onWin() {
        env = (Integer.parseInt(String.valueOf(score.getText())) + (Integer.parseInt(String.valueOf(schet.getText())) * 2));
        score.setText(Integer.toString(env));
    }
        // Отнимаем при проигрыше
    public void onLose() {
        env = ((Integer.parseInt(String.valueOf(score.getText())) - (Integer.parseInt(String.valueOf(schet.getText())))));
        score.setText(Integer.toString(env));
    }

    // Запоминаем данные при закрытии
    @Override
    protected void onPause() {
        super.onPause();
    }

    String cn(int stt) {
        String win = "";
        // Хитрая схема
        if (stt >= (Factor * 1) && stt < (Factor * 3)) {
            if (e == 32) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 3) && stt < (Factor * 5)) {
            if (e == 15) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 5) && stt < (Factor * 7)) {
            if (e == 19) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 7) && stt < (Factor * 9)) {
            if (e == 4) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 9) && stt < (Factor * 11)) {
            if (e == 21) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 11) && stt < (Factor * 13)) {
            if (e == 2) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 13) && stt < (Factor * 15)) {
            if (e == 25) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 15) && stt < (Factor * 17)) {
            if (e == 17) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 17) && stt < (Factor * 19)) {
            if (e == 34) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 19) && stt < (Factor * 21)) {
            if (e == 6) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 21) && stt < (Factor * 23)) {
            if (e == 27) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 23) && stt < (Factor * 25)) {
            if (e == 13) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 25) && stt < (Factor * 27)) {
            if (e == 36) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 27) && stt < (Factor * 29)) {
            if (e == 11) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 29) && stt < (Factor * 31)) {
            if (e == 30) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 31) && stt < (Factor * 33)) {
            if (e == 8) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 33) && stt < (Factor * 35)) {
            if (e == 23) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 35) && stt < (Factor * 37)) {
            if (e == 10) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 37) && stt < (Factor * 39)) {
            if (e == 5) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 39) && stt < (Factor * 41)) {
            if (e == 24) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 41) && stt < (Factor * 43)) {
            if (e == 16) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 43) && stt < (Factor * 45)) {
            if (e == 33) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 45) && stt < (Factor * 47)) {
            if (e == 1) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 47) && stt < (Factor * 49)) {
            if (e == 20) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 49) && stt < (Factor * 51)) {
            if (e == 14) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 51) && stt < (Factor * 53)) {
            if (e == 31) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 53) && stt < (Factor * 55)) {
            if (e == 9) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 55) && stt < (Factor * 57)) {
            if (e == 22) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 57) && stt < (Factor * 59)) {
            if (e == 18) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 59) && stt < (Factor * 61)) {
            if (e == 29) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 61) && stt < (Factor * 63)) {
            if (e == 7) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 63) && stt < (Factor * 65)) {
            if (e == 28) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 65) && stt < (Factor * 67)) {
            if (e == 12) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 67) && stt < (Factor * 69)) {
            if (e == 35) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 69) && stt < (Factor * 71)) {
            if (e == 3) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if (stt >= (Factor * 71) && stt < (Factor * 73)) {
            if (e == 26) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        if ((stt >= (Factor * 73) && stt < 360) || (stt >= 0 && stt < (Factor * 1))) {
            if (e == 0) {
                win = "Вы выйграли";
            } else {
                win = "Вы проиграли";
            }
        }
        // возвращение значение
        return win;
    }

}



